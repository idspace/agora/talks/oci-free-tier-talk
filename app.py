from flask import Flask, jsonify
from flask_cors import CORS  # Import the CORS module
import oracledb

cs='''(description = (retry_count=20)(retry_delay=3)(address=(protocol=tcps)
     (port=1522)(host=adb.eu-frankfurt-1.oraclecloud.com))
     (connect_data=(service_name=g43b2b20523a970_oraclemasterclass_high.adb.oraclecloud.com))
     (security=(ssl_server_dn_match=yes)))'''


app = Flask(__name__)

CORS(app)  # Enable CORS for all routes

@app.route('/movies', methods=['GET'])
def get_movies():

    connection=oracledb.connect(
         user="oracle_masterclass",
         password="OracleMasterclass123",
         dsn=cs)

    cursor = connection.cursor()

    rows = cursor.execute('select title, imdb_rating, year from movies_1')

    movies_data = [{'title': row[0], 'rating': row[1], 'year': row[2]} for row in rows]

    cursor.close()

    connection.close()

    return jsonify({'movies': movies_data})

if __name__ == '__main__':
    # Run the Flask app on localhost:5000
    app.run(host='0.0.0.0', port=5000, debug=True)
